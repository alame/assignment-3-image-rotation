#include "Image.h"
#include <malloc.h>
struct image* rotate(struct image* image, struct image* new){
    if (image == NULL){
        new = NULL;
        return new;
    }
    init_image(image->height, image->width, new);

    for (uint32_t i=0; i< image->height;i++){
        for (uint32_t j =0; j<image->width; j++){
            new->data[(image->width-j-1)*new->width+i] = image->data[i*image->width+j];
        }
    }
    del_image(image);
    return new;
}

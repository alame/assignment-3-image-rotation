#include <stdio.h>
enum close_file_status  {
    CLOSE_OK = 0,
    CLOSE_FAIL
};
FILE *openToRead(char fileName[]){
    return fopen(fileName, "rb");
}
FILE *openToWrite(char fileName[]){
    return fopen(fileName, "wb");
}
enum close_file_status close(FILE* file){
    int result = fclose(file);
    if(result==0) return CLOSE_OK;
    return CLOSE_FAIL;
}

#include <BmpHeader.h>
#include <Image.h>
#include <stdio.h>
#include <utils.h>

enum read_status  {
    READ_OK = 0,
    READ_FAIL

};

#define SIZEOF_STRUCT_BMP_HEADER sizeof(struct bmp_header)
#define SIZEOF_STRUCT_PIXEL sizeof(struct pixel)
enum read_status from_bmp(FILE *in, struct image *img){
    struct bmp_header header;
    if(fread(&header, SIZEOF_STRUCT_BMP_HEADER,1, in)!=1) {
        return READ_FAIL;
    }
    if (header.bfType != 19778){
        return READ_FAIL;
    }
    init_image(header.biWidth, header.biHeight, img);
    if (img == NULL){
        return READ_FAIL;
    }
    long padding = calc_padding(img->width);
    for (uint32_t i=0; i<img->height;i++){
        if(fread(&img->data[i * header.biWidth], SIZEOF_STRUCT_PIXEL, header.biWidth, in)!=header.biWidth){
            return READ_FAIL;
        }
        fseek(in, padding, SEEK_CUR);
    }
    return READ_OK;

}

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum write_status to_bmp( FILE* out, struct image const* img ){
    long padding = calc_padding(img->width);
    uint32_t row_size = img->width*SIZEOF_STRUCT_PIXEL +padding;
    struct bmp_header header = {
            .bfType = 19778,
            .biWidth = img->width,
            .biHeight = img->height,
            .biSizeImage = row_size * img->height,
            .bOffBits = SIZEOF_STRUCT_BMP_HEADER,
            .bfileSize = SIZEOF_STRUCT_BMP_HEADER + row_size * img->height,
            .bfReserved = 0,
            .biSize = 40,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biXPelsPerMeter = 2835,
            .biYPelsPerMeter = 2835,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
    if (fwrite(&header, SIZEOF_STRUCT_BMP_HEADER, 1, out) != 1) {
        return WRITE_ERROR;
    }
    for (uint32_t i=0;i<img->height;i++){
        if (fwrite(&img->data[i*img->width], SIZEOF_STRUCT_PIXEL, img->width, out) != img->width){
            return WRITE_ERROR;
        }
        fseek(out, padding, SEEK_CUR);
    }
    return WRITE_OK;
}


#include <malloc.h>
#include <stdint.h>
struct pixel{
    uint8_t r,g,b;
};
struct image {
    uint64_t width, height;
    struct pixel* data;
};
void init_image(uint64_t width, uint64_t height, struct image* img) {
    *img = (struct image){
        .height = height,
        .width = width,
        .data = malloc(sizeof (struct pixel) * width * height)
    };
    if( img->data == NULL){
        free(img->data);
        img = NULL;
    }
}
void del_image(struct image* img) {
    free(img->data);
    img->data = NULL;
}


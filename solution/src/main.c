#include <BmpReader.h>
#include <iolib.h>

#include <rotate.h>
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char** argv) {
    if (argc != 4){
        fprintf(stderr, "%s\n", "should be 4 arguments");
        return 1;
    }
    FILE *file = openToRead(argv[1]);
    if (file==NULL){
        fprintf( stderr,"%s\n", "can't open file to read");
        return 1;
    }
    struct image image;
    enum read_status  readStatus = from_bmp(file, &image);
    if (readStatus==READ_FAIL){
        fprintf(stderr, "%s\n", "can't read file");
        close(file);
        return 1;
    }
    enum close_file_status closeFileStatus = close(file);
    if (closeFileStatus ==CLOSE_FAIL){
        fprintf(stderr, "%s\n", "can't close file");
    }
    int angle = atoi(argv[3]);
    if (angle%90!=0){
        fprintf(stderr, "%s\n", "invalid angle");
        return 1;
    }
    if (angle < 0) angle = 360 + angle;
    int rotation_number = angle / 90 %4;
    struct image rotated_image;
    struct image* new_image = &image;
    for (uint32_t i=0; i<rotation_number; i++){
        new_image = rotate(&image, &rotated_image);
        if (new_image==NULL){
            fprintf(stderr, "%s\n", "out of memory");
            return 1;
        }
        image = *new_image;
    }
    FILE *outFile = openToWrite(argv[2]);
    if (outFile == NULL){
        fprintf(stderr, "%s\n", "can't open out file to write");
        return 1;
    }
    enum write_status writeStatus = to_bmp(outFile, new_image);
    del_image(new_image);
    if (writeStatus == WRITE_ERROR){
        fprintf(stderr, "%s\n", "can't write in out file");
    }
    closeFileStatus = close(outFile);
    if (closeFileStatus ==CLOSE_FAIL){
        fprintf(stderr, "%s\n", "can't close out file");
    }
    return 0;
}

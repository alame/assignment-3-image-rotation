

#ifndef IMAGE_TRANSFORMER_IOLIB_H
#define IMAGE_TRANSFORMER_IOLIB_H

#include <stdio.h>
enum close_file_status  {
    CLOSE_OK = 0,
    CLOSE_FAIL
};
FILE * openToRead(char fileName[]);
enum close_file_status close(FILE* file);
FILE *openToWrite(char fileName[]);
#endif

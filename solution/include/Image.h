



#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include <stdint.h>
struct pixel{
    uint8_t r,g,b;
};
struct image {
    uint64_t width, height;
    struct pixel* data;
};
void init_image(uint64_t width, uint64_t height, struct image* img);
void del_image(struct image* img);
#endif

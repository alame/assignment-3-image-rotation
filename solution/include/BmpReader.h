#include <Image.h>
#include <stdio.h>

#ifndef IMAGE_TRANSFORMER_BMPREADER_H
#define IMAGE_TRANSFORMER_BMPREADER_H

enum read_status  {
    READ_OK = 0,
    READ_FAIL

};

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};
enum read_status from_bmp(FILE *in, struct image *img);
enum write_status to_bmp( FILE* out, struct image const* img );

#endif
